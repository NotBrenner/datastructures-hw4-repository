//
//  main.cpp
//  data structures homework 4
//
//  Created by Rob MacDonald on 10/24/16.
//  Copyright © 2016 Rob MacDonald. All rights reserved.
//

#include <iostream>
#include "Airport class.hpp"

using namespace std;

int main() {
    
    char x[128];
    
    cout << "Starting the first simulation." << endl;
    
    Airport(5, 2, 0.1, 0.2, 45, 120);
    
    cout << "First simulation finished (press enter to continue)" << endl;
    
    cin >> x;
    
    cout << "Starting the second simulation." << endl;
    
    Airport(15, 10, 0.9, 0.9, 30, 1440);
    
    cout << "Second simulation finished" << endl;
    
    return 0;
}



//
//  airport class.cpp
//  data structures homework 4
//
//  Created by Rob MacDonald on 10/24/16.
//  Copyright © 2016 Rob MacDonald. All rights reserved.
//

#include "airport class.hpp"// Includes the library header file for the class, which connects all the functions initializations to the actual functions and allows them to work in seperate files

#include <queue>//Includes the queue library, which then gives access to using queue and all functions that correlate with this command

#include <cstdlib>//The standard library, used for generally arithmetic and conversion functions

#include <iostream>// The basic c++ library allowing simple functions such as cout and all initializations of variables such as int and double

using namespace std;// standard c++ libray, allows the user to not have to type std:: before every function

void Airport(//allows for the user to input into the void airport functions and give it the ability to do whatever the function needs to do.
             
             int landingTime, // Time segments needed for one plane to land
             
             int takeoffTime, // Time segs. needed for one plane to take off
             
             double arrivalProb, // Probability that a plane will arrive in
             // any given segment to the landing queue
             
             double takeoffProb, // Probability that a plane will arrive in
             // any given segment to the takeoff queue
             
             int maxTime, // Maximum number of time segments that a plane
             // can stay in the landing queue
             
             int simulationLength// Total number of time segs. to simulate
) {
    queue<int> takeoffQ, landingQ;//creates two empty queues named takeoffQ and landingQ
    
    int landingWaits = 0, takeoffWaits = 0, timeFree = 0, crashes = 0;//initializes four integer variables name landingWaits, takeoffWaits, timeFree, and crashes all to zero.
    
    int numLandings = 0, numTakeoffs = 0;//Initializes two more integer variables numLandings and numTakeoffs to zero
    
    srand(time(NULL)); //Sets the random number generator to whatever time will be pointing at, while time is is currently set to NULL for the random value
    
    for (int curTime = 0; curTime < simulationLength; curTime++)//Creates a for loop that runs through the curTime variable until it reaches the simulationlength, and icrements curTime to one each time so the loop can run through each scenario in the simulation before the end of it.
    {
        
        if (((double) rand() / (double) RAND_MAX) < arrivalProb)//If the random value divided by the max random value the simuation can be is less then the arrival Prob, which stands for the time in which the first airplane may arrive
        {
            
            landingQ.push(curTime);//pushes the landing Q time to the current time, because that means it will land when curTime reaches that number
            
            cout << "Arrival Time" << endl;
            
            cout << curTime <<endl;
            
        }
        if (((double) rand() / (double) RAND_MAX) < takeoffProb)//Iinitalizes another if statement saying if the random value at curTime divided by the random max is less than the takeoffProb
        {
            
            takeoffQ.push(curTime);//pushes the takeoff time to curTime, beacuse the plane will be taking off at this time before the simulation ends
            
            cout << "Take off time" << endl;
            
            cout<< curTime << endl;
            
        }
        
        if (timeFree <= curTime)//Initializes an if statement saying if timeFree is less than or equal to curTime
        {
            
            if (!landingQ.empty())//Nested if saying if the landingQ is empty
            {
                
                if ((curTime - landingQ.front()) > maxTime)//Nested loop saying if curTime minus landingQ.front is greater than the max time
                {
                    
                    crashes++;// if all the if statements boil down, then the simulation will end with a crash beacuse the empty zone of for landing was not open in time for the curTime to land
                    
                    landingQ.pop();//This landing Q will pop off the top and be replaced with a new value because the old value crashed and did not work.
                    
                    cout << "how many crashes" << endl;
                    
                    cout << crashes << endl;
                }
                else//initalizes an else statement after the previous ifs
                {
                    
                    landingWaits += (curTime - landingQ.front());//assigns landingWaits to curTime - landingQfront everytime the loop runs through
                    
                    landingQ.pop();//Pops off the new value of landingQ in the queue
                    
                    timeFree += landingTime;//Incrememnts timeFree to landing time, allowing the planes to be able to land safely now that the waits will run through the new value of landing time every time the landingQ value is popped
                    
                    numLandings++;//Increments a new number of landngs everytime the loop runs correctly through the else statement
                    
                    cout << "number of landings" << endl;
                    
                    cout<< numLandings << endl;
                }
                
            }
            else if (!takeoffQ.empty())//Starts a new else if statement
                
            {
                
                takeoffWaits += (curTime - takeoffQ.front());//If takeoffWaits increments to the curTime minus the front value of the queue for takeoffQ
                
                takeoffQ.pop();//pops off the front of takeoffQ as it has now being assigned to the variable takeoffWaits
                
                timeFree += takeoffTime;//The timeFree variable is now assigned to the takeOfftime, and now every plane will be able to take off at the appropriate tie
                
                numTakeoffs++;//incremements another number into the numTakeoffs
                
                cout << "number of takeoffs" << endl;
                
                cout << numTakeoffs << endl;
                
            }
            
        }
        
    }
    
    while (!landingQ.empty() && (simulationLength - landingQ.front()) > maxTime)//if the empty spot of the landingQ and the simulation length - the front of the landingQ is greater than the max time
    {
        
        crashes++;//the plane will then crash because the it runs over the max time of the simulation and then wont have a landing time appropriate for the plane
        
        landingQ.pop();//pops off that value because the top value crashes and replaces it with a new one
    }
    
    cout << "Number of crashes: " << crashes << "\n";
    
    cout << "Number of takeoffs: " << numTakeoffs << " (avg delay " <<
    ((double) takeoffWaits) / ((double) numTakeoffs) << ")\n";
    
    cout << "Number of landings: " << numLandings << " (avg delay " <<
    ((double) landingWaits) / ((double) numLandings) << ")\n";
    
    cout << "Number of planes in takeoff queue: " << takeoffQ.size() << "\n";
    
    cout << "Number of planes in landing queue: " << landingQ.size() << "\n";
    
}













//
//  airport class.hpp
//  data structures homework 4
//
//  Created by Rob MacDonald on 10/24/16.
//  Copyright © 2016 Rob MacDonald. All rights reserved.
//

#ifndef airport_class_hpp

#define airport_class_hpp

#include <stdio.h>

void Airport(
             
             int landingTime,    // Time segments needed for one plane to land
             
             int takeoffTime,    // Time segs. needed for one plane to take off
             
             double arrivalProb, // Probability that a plane will arrive in
             // any given segment to the landing queue
             
             double takeoffProb, // Probability that a plane will arrive in
             // any given segment to the takeoff queue
             
             int maxTime,        // Maximum number of time segments that a plane
             // can stay in the landing queue
             
             int simulationLength// Total number of time segs. to simulate
);

#endif /* airport_class_hpp */










